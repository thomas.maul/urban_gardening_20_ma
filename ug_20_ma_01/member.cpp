#include "member.h"

Member::Member()
{
    memberId = -1;
    isBoardMember = false;
    patch = nullptr;
    leaseRequest = leaseRequestNone;
}

Member::Member(int newMemberId, QString newName, QString newPassword, bool newBoardMember, Patch *newPatch, leaseRequestType newLeaseRequest)
{
    memberId = newMemberId;
    isBoardMember = newBoardMember;
    patch = newPatch;
    leaseRequest = newLeaseRequest;
    password = newPassword;
    name = newName;
}
QString Member::getName()
{
    return name;
}

void Member::setName(QString newName)
{
    name = newName;
}

int Member::getMemberId()
{
    return memberId;
}

void Member::setMemberId(int newMemberId)
{
    memberId = newMemberId;
}

bool Member::getIsBoardMember()
{
    return isBoardMember;
}

void Member::setIsBoardMember(bool newVorstand)
{
    isBoardMember = newVorstand;
}

Patch *Member::getPatch() const
{
    return patch;
}

void Member::setPatch(Patch *newPatch)
{
    patch = newPatch;
}

Member::leaseRequestType Member::getLeaseRequest()
{
    return leaseRequest;
}

void Member::setLeaseRequest(leaseRequestType newLeaseRequest)
{
    leaseRequest = newLeaseRequest;
}

QString Member::getLeaseRequestString(leaseRequestType newLeaseRequest)
{
    QString retString;

    switch (newLeaseRequest) {
    case leaseRequestNone:
        retString = "keine Anfrage";
        break;

    case leaseRequestRequestet:
        retString = "Anfrage gestellt";
        break;

    case leaseRequestConfirmed:
        retString = "Anfrage bestätigt";
        break;
    case leaseRequestDenied:
        retString = "Anfrage abgelehnt";
        break;

    case leaseRequestUnknown:
        retString = "Anfrage Status unbekannt";
        break;
    }
    return retString;
}

QString Member::getPassword() const
{
    return password;
}

void Member::setPassword(const QString &newPassword)
{
    password = newPassword;
}
