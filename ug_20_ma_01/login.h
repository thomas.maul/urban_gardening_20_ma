#ifndef LOGIN_H
#define LOGIN_H

/**
 * @class Login
 *
 * - Passwort in Datei
 * - Passwort verschlüsseln
 *   - Cäsar-Code
 *   - Cäsar-Code min CBC-Mode
 */

#include <QString>

class Login
{
public:
    Login();

    bool loadPwFile(QString fileName);
    static QString cryptCeasarSimple(QString plainText, unsigned short shift);
    static QString decryptCeasarSimple(QString plainText, unsigned short shift);
    static QString cryptCaesarCbC(QString plainText, unsigned short shift);
    static QString decryptCaesarCbC(QString plainText, unsigned short shift);
};

#endif // LOGIN_H
