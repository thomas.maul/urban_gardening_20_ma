#include "login.h"

Login::Login()
{

}

bool Login::loadPwFile(QString fileName)
{

}

/**
 * @brief Login::cryptCeasarSimple
 * @param plainText
 * @param shift
 * @return
 *
 * plainTextChar ist Array von Char (pointer auf das Array). Letztes Zeichen ist \0 (binär null).
 */
QString Login::cryptCeasarSimple(QString plainText, unsigned short shift)
{
    QByteArray plainTextChar = plainText.toLatin1();
    QString retString;

    for(int i = 0; i < plainText.length(); i++)
    {
        plainTextChar[i] +=  shift;
        if((plainTextChar[i] > 0x5A && plainTextChar[i] < 0x61) ||
            plainTextChar[i] > 0x7A)
            plainTextChar[i] -=26;
    }
    retString = plainTextChar;
    return retString;
}

QString Login::decryptCeasarSimple(QString plainText, unsigned short shift)
{
    QByteArray plainTextChar = plainText.toLatin1();
    QString retString;

    for(int i = 0; i < plainText.length(); i++)
    {
        plainTextChar[i] -=  shift;
        if(plainTextChar[i] < 0x41 ||
            (plainTextChar[i] > 0x5A && plainTextChar[i] < 0x61))
            plainTextChar[i] +=26;
    }
    retString = plainTextChar;
    return retString;
}

QString Login::cryptCaesarCbC(QString plainText, unsigned short shift)
{
    QByteArray plainTextChar = plainText.toLatin1();
    QString retString;

    for(int i = 0; i < plainText.length(); i++)
    {
        plainTextChar[i] +=  shift;
        if((plainTextChar[i] > 0x5A && plainTextChar[i] < 0x61) ||
            plainTextChar[i] > 0x7A)
            plainTextChar[i] -=26;
        shift = plainTextChar[i];
    }
    retString = plainTextChar;
    return retString;
}

QString Login::decryptCaesarCbC(QString plainText, unsigned short shift)
{
    QByteArray plainTextChar = plainText.toLatin1();
    QString retString;

    for(int i = 0; i < plainText.length(); i++)
    {
        plainTextChar[i] -=  shift;
        if(plainTextChar[i] < 0x41 ||
            (plainTextChar[i] > 0x5A && plainTextChar[i] < 0x61))
            plainTextChar[i] +=26;
        shift = plainTextChar[i];
    }
    retString = plainTextChar;
    return retString;
}
