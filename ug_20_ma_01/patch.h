#ifndef PATCH_H
#define PATCH_H


class Patch
{
public:
    enum statusPatchType {statusFree, statusRequested, statusLeased, statusUnknown = -1};
    Patch();
    Patch(int newPatchId, int newField, unsigned short newQuality, statusPatchType newStatusPatch);
    int getPatchId();
    void setPatchId(int newPatchId);

    int getField();
    void setField(int newField);

    unsigned short getQuality();
    void setQuality(unsigned short newQuality);

    statusPatchType getStatus();
    void setStatus(statusPatchType newStatusPatch);

private:
    int patchId;
    int field;
    unsigned short quality;
    statusPatchType statusPatch;
};

#endif // PATCH_H
