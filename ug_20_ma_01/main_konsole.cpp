/**
 * @main Komanduzeilenargument: g für GUI, k für Konsole.
 * Konsole funtioniert in Windows in diesem Programm nicht!
 */

#include <iostream>
#include <string>

#include "association.h"
#include "testcreatememberspatches.h"

using namespace std;

void anzeigemenu()
{
    cout << "Menu urban gardening 2.0" << endl
         << endl
         << "Was wollen Sie machen?" << endl
         << "1 Anzeige ein Beet" << endl
         << "2 Anzeige alle Beete" << endl
         << "3 Anzeige alle Mitglieder" << endl
         << "4 Ein Beet pachten (Antrag)" << endl << endl
         << "5 Antrag bestaetigen - nur Vorstand" << endl
         << " ----------- " << endl << endl
         << "g Testdaten generieren" << endl
         << "e Programm beeenden" << endl;
}

void mainLoop()
{
    char auswahlMenu = 0;
    int patchId = -1;
    int patchIdRet = -1;
    int memberId = -1;
    int memberBoardId = -1;
    int fieldId = -1;
    bool confirm = false;
    unsigned short quality = 0;
    vector<Patch *> patches;
    vector<Member *> members;
    Member::leaseRequestType leaseRequest = Member::leaseRequestUnknown;
    string leaseRequestString;

    /// Zugriff auf das Objeklt vom Verein ueban Gardening 2.0
    Association *ug20;

    /// Test-Klasse zum Anlegen von Usern, Beeten und spaeter zum Testen.
    TestCreateMembersPatches *myTestMemersPatches;

    ug20 = new Association;
    myTestMemersPatches = new TestCreateMembersPatches(ug20);

    while('e' != auswahlMenu )
    {
        anzeigemenu();
        cin >> auswahlMenu;

        switch (auswahlMenu) {
        case '1':
            cout << "Welches Beet?" << endl;
            cin >> patchId;
            ug20->showPatchSingle(patchId, patchIdRet, fieldId, quality);
            cout << "Beet: " << patchIdRet << " Acker: " << fieldId << " Qualitaet: " << quality << endl;
            break;

        case '2':
            ug20->showPatchesAll(patches);
            for(unsigned long long i = 0; i < patches.size(); i++)
            {
                cout << "Beet: " << patches[i]->getPatchId() << " Acker: " << patches[i]->getField() << " Qualitaet: " << patches[i]->getQuality() << endl;
            }
            break;

        case '3':
            ug20->showMembersAll(members);
            for(unsigned long long i = 0; i < members.size(); i++)
            {
                leaseRequest = members[i]->getLeaseRequest();
                leaseRequestString = Member::getLeaseRequestString(leaseRequest).toStdString();

                cout << "MitglNr: " << members[i]->getMemberId()
                     << " Name: " << members[i]->getName().toStdString()
                     << " Vorstand: " << members[i]->getIsBoardMember()
                     << " Beet: " << members[i]->getPatch()
                     << " Antrag: " << leaseRequestString
                     << endl;
            }
            break;

        case '4':
            cout << "Welches Beet?" << endl;
            cin >> patchId;
            cout << "Deine MitgleidsNr?" << endl;
            cin >> memberId;

            if(ug20->leasePatch(patchId, memberId))
                cout << "Antrag gestellt.";
            else
                cout << "Antrag hat nicht funktioniert.";
            break;

        case '5':
            cout << "Beet Antrag bestätigen" << endl;
                        cin >> patchId;
            cout << "Fuer welches Mitglied (Nr)?" << endl;
            cin >> memberId;
            cout << "Deine MitgliedsNr (Vorstand)" << endl;
            cin >> memberBoardId;
            cout << "Antrag bestaetigen? (j/n)";
            cin >> auswahlMenu;
            if('j' == auswahlMenu || 'J' == auswahlMenu)
                confirm = true;
            else
                confirm = false;
            if(ug20->handleApplicationLeasePatch(memberId, memberBoardId, confirm))
                cout << "Antrag bestaetigt.";
            else
                cout << "Antrag hat nicht funktioniert.";
            break;
        case 'g':
            myTestMemersPatches->testgenerateValidePatches();
            myTestMemersPatches->testGenerateValideMembers();
            break;
            //        case '':

            //            break;
            //        case '':

            //            break;
        default:
            break;
        }
    }
}

/**
 * main-Funktion mit Schalter Konsole / GUI
 * Schalter funktioniert bei Windows nur auf GUI.
 * Konolen-Main für Windows ist main-konsole.cpp, im Projektbaum
 * Alle Klasen bleiben identisch.
 */
int main(int argc, char *argv[])
{
    mainLoop();
    cout << "Bis bald" << endl;
    return 0;
}
