#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    ug20 = new Association;
    myTestMemersPatches = new TestCreateMembersPatches(ug20);

    connect(ui->btnShowSinglePatch, &QPushButton::clicked, this, &Widget::showSinglePatch);
    connect(ui->btnCreateTestSet1, &QPushButton::clicked, this, &Widget::createTestSet1);
    connect(ui->btnLeasePatch, &QPushButton::clicked, this, &Widget::leaseRequest);
    connect(ui->btnLeaseConfirm, &QPushButton::clicked, this, &Widget::leaseConfirm);
    connect(ui->btnShowPatchesAll, &QPushButton::clicked, this, &Widget::showPatchesAll);
    connect(ui->btnShowMembersAll, &QPushButton::clicked, this, &Widget::showMembersAll);
    connect(ui->btnCryptCaesar, &QPushButton::clicked, this, &Widget::cryptByCaesarCodeSimple);
    connect(ui->btnDecryptCaesar, &QPushButton::clicked, this, &Widget::decryptByCaesarCodeSimple);
}

Widget::~Widget()
{
    delete ui;
}


void Widget::showSinglePatch()
{
    int patchIdIn = -1;
    int patchId = -1;
    int fieldId = -1;
    unsigned short quality = 0;

    patchIdIn = ui->spinPatchIn->value();

    ug20->showPatchSingle(patchIdIn, patchId, fieldId, quality);

    ui->spinPatchId->setReadOnly(true);
    ui->spinFieldId->setReadOnly(true);
    ui->spinQuality->setReadOnly(true);

    ui->spinPatchId->setValue(patchId);
    ui->spinFieldId->setValue(fieldId);
    ui->spinQuality->setValue(quality);
}


void Widget::createTestSet1()
{
    myTestMemersPatches->testgenerateValidePatches();
    myTestMemersPatches->testGenerateValideMembers();
}

void Widget::leaseRequest()
{
    bool ok;
    int memberId = QInputDialog::getInt(this, tr("Beet pachten"),
                                        tr("Gib bitte deine Mitgliedsnummer ein"), -1, 0, 100, 1, &ok);
    int requestPatchId = QInputDialog::getInt(this, tr("Beet pachten"),
                                              tr("Welches Beet willst du pachten?"), -1, 0, 100, 1, &ok);

    if( 0 <= requestPatchId && Association::maxCountPatchesMembers > requestPatchId)
        ug20->leasePatch(memberId, requestPatchId);
}

void Widget::leaseConfirm()
{
    int member = -1;
    int memberBoard = -1;
    bool confirmRequest = false;

    member = ui->spinMemberLease->value();
    memberBoard = ui->spinMemberBoard->value();

    if(ui->rbConfirmLeaseRequest->isChecked())
        confirmRequest = true;
    else
        confirmRequest = false;
    ug20->handleApplicationLeasePatch(member, memberBoard, confirmRequest);
}

void Widget::showPatchesAll()
{
    vector<Patch*> tempPatches;
    QTableWidgetItem* tempItem = nullptr;

    ug20->showPatchesAll(tempPatches);

    ui->tableWidgetPatches->clearContents();
    ui->tableWidgetPatches->setRowCount(tempPatches.size());
    for(unsigned long i = 0; i < tempPatches.size(); i++) {
        tempItem = new QTableWidgetItem;
        tempItem->setText(QString::number(tempPatches[i]->getPatchId()));
        ui->tableWidgetPatches->setItem(i, 0, tempItem);
        tempItem = new QTableWidgetItem;
        tempItem->setText(QString::number(tempPatches[i]->getField()));
        ui->tableWidgetPatches->setItem(i, 1, tempItem);
        tempItem = new QTableWidgetItem;
        tempItem->setText(QString::number(tempPatches[i]->getQuality()));
        ui->tableWidgetPatches->setItem(i, 2, tempItem);
    }
}

void Widget::showMembersAll()
{
    vector<Member*> tempMembers;
    QTableWidgetItem* tempItem = nullptr;
    Member::leaseRequestType tempLeaseRequest = Member::leaseRequestUnknown;
    QString tempLeaseRequestString;

    ug20->showMembersAll(tempMembers);

    ui->tableWidgetMembers->clearContents();
    ui->tableWidgetMembers->setRowCount(tempMembers.size());
    for(unsigned long i = 0; i < tempMembers.size(); i++) {
        tempItem = new QTableWidgetItem;
        tempItem->setText(QString::number(tempMembers[i]->getMemberId()));
        ui->tableWidgetMembers->setItem(i, 0, tempItem);

        tempItem = new QTableWidgetItem;
        tempItem->setText(tempMembers[i]->getName());
        ui->tableWidgetMembers->setItem(i, 1, tempItem);

        tempItem = new QTableWidgetItem;
        tempItem->setText(QString::number(tempMembers[i]->getIsBoardMember()));
        ui->tableWidgetMembers->setItem(i, 2, tempItem);

        tempItem = new QTableWidgetItem;
        if(nullptr != tempMembers[i]->getPatch())
            tempItem->setText(QString::number(tempMembers[i]->getPatch()->getPatchId()));
        ui->tableWidgetMembers->setItem(i, 3, tempItem);

        tempItem = new QTableWidgetItem;
        tempLeaseRequest = tempMembers[i]->getLeaseRequest();
        tempLeaseRequestString = Member::getLeaseRequestString(tempLeaseRequest);
        tempItem->setText(tempLeaseRequestString);
        ui->tableWidgetMembers->setItem(i, 4, tempItem);
    }
}

void Widget::cryptByCaesarCodeSimple()
{
    QString plainText;
    QString cipherText;

    plainText = QInputDialog::getText(this, tr("Login"),
                                      tr("Password:"), QLineEdit::Normal);
    if(ui->rbCaesarPlain->isChecked())
        cipherText = Login::cryptCeasarSimple(plainText, 5);
    if(ui->rbCaesarCbc->isChecked())
        cipherText = Login::cryptCaesarCbC(plainText, 5);

    QMessageBox::information(this, tr("Password"), "Your PW: " + cipherText);
}

void Widget::decryptByCaesarCodeSimple()
{
    QString plainText;
    QString cipherText;

    cipherText = QInputDialog::getText(this, tr("encrypted PW"),
                                       tr("Password:"), QLineEdit::Normal);
    if(ui->rbCaesarPlain->isChecked())
        cipherText = Login::decryptCeasarSimple(plainText, 5);
    if(ui->rbCaesarCbc->isChecked())
        cipherText = Login::decryptCaesarCbC(plainText, 5);

    QMessageBox::information(this, tr("Password"), "Your PW: " + plainText);
}

