#ifndef TESTCREATEMEMBERSPATCHES_H
#define TESTCREATEMEMBERSPATCHES_H

#include "association.h"

class TestCreateMembersPatches
{
public:
    TestCreateMembersPatches(Association *newVerein);
    bool testGenerateValideMembers();
    bool testgenerateValidePatches();
private:
    Association *ug20Tester;
};

#endif // TESTCREATEMEMBERSPATCHES_H
