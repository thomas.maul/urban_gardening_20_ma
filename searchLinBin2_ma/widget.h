#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "ug21.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private:
    Ui::Widget *ui;
    Ug21 ug21;

private slots:
    void findLin();
    void findBin();
    void shuffle();
};
#endif // WIDGET_H
