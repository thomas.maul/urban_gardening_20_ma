#include "ug21.h"

Ug21::Ug21()
{
    for(int i = 0; i < maxCountPatches; i++)
    {
        patches[i] = new Patch(i, i%10, 3, Patch::statusFree);
    }

}

void Ug21::sortArray()
{

}

int Ug21::findLinPatch(int patchId, int &fieldId)
{
    for(int i = 0;  i < maxCountPatches; i++)
    {
        if(patches[i]->getPatchId() == patchId)
        {
            fieldId = patches[i]->getField();
            return true;
        }
    }
    return false;
}

bool Ug21::findBinPatch(int patchId, int &fieldId)
{
    unsigned short pivot = maxCountPatches/2;
    unsigned short left = 0;
    unsigned short right = maxCountPatches - 1;


    if(patches[pivot]->getPatchId() == patchId)
    {
        fieldId = patches[pivot]->getField();
        return true;
    }

    while (   patches[pivot]->getPatchId() != patchId
           && right >= left)
    {
        if(patches[pivot]->getPatchId() > patchId)
        {
            // links weitersuchen
            right = pivot - 1;
            pivot = (right - left)/2 + left;

            if(patches[pivot]->getPatchId() == patchId)
            {
                fieldId = patches[pivot]->getField();
                return true;
            }
        }
        else
        {
            // rechts weitersuchen
            left = pivot + 1;
            pivot = (right - left)/2 + left;
            if(patches[pivot]->getPatchId() == patchId)
            {
                fieldId = patches[pivot]->getField();
                return true;
            }
        }
    }
    return false;
}

Patch *Ug21::getPatches() const
{
    return *patches;
}


void Ug21::initPatcheslin()
{
    for(int i = 0; i < maxCountPatches; i++)
    {
        patches[i]->setPatchId(i);
    }
}

void Ug21::initPatchesRandom()
{
    for(int i = 0; i < maxCountPatches; i++)
    {
        patches[i]->setPatchId(rand()%100);
    }
}
