#include <iostream>
#include "ug21.h"

using namespace std;

int main()
{
    char selection = 0;
    int patchId = -1;
    int fieldId = -1;
    Ug21 ug21;
    cout << "Suche" << endl << endl;

    while ('q' != selection)
    {
        cout << "Was willst du machen?" << endl
             << "m mischen" << endl
             << "l lineare Suche" << endl
             << "b binaere Suche" << endl
             << "q Programm Ende" << endl;
        cin >> selection;

        switch (selection)
        {
        case 'm':
            ug21.initPatchesRandom();
            break;
        case 'l':
            cout << "was suchst de? Nr von Beet" << endl;
            cin >> patchId;

            if(ug21.findLinPatch(patchId, fieldId))
            {
                cout << "Gefunden!" << endl;
                cout << patchId << " " << fieldId << endl;
            }
            else
                cout << "net do" << endl;
            break;
        case 'b':
            cout << "was suchst de? Nr von Beet" << endl;
            cin >> patchId;

            if(ug21.findBinPatch(patchId, fieldId))
            {
                cout << "Gefunden!" << endl;
                cout << patchId << " " << fieldId << endl;
            }
            else
                cout << "net do" << endl;
            break;
        default:
            cout << "Gibts net, hammer net. Probiers nochma" << endl;
            break;
        }
    }

    return 0;
}
