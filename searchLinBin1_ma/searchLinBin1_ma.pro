TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
        patch.cpp \
        ug21.cpp

HEADERS += \
    patch.h \
    ug21.h
