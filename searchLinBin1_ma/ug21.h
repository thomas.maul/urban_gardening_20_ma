#ifndef UG21_H
#define UG21_H

#include <random>
#include "patch.h"


class Ug21
{
public:
    static const unsigned short maxCountPatches = 100;
    Ug21();

    void sortArray();
    int findLinPatch(int patchId, int &fieldId);
    bool findBinPatch(int patchId, int &fieldId);
    Patch *getPatches() const;
    void initPatcheslin();
    void initPatchesRandom();
private:
    Patch *patches[maxCountPatches];

};

#endif // UG21_H
